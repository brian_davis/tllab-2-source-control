#include <iostream>   // Use cin and cout
#include <string>     // Use strings
using namespace std;

void Program1()  // I am editing this code.
{
    string name ;  // I am also editing here.
    string hometown ;  // I was told to edit.  So, I am editing.
    string hometown2 ;  // The date and time is 2020/06/08 16:50
    cout << "Enter your hometown: " ;
    cin >> hometown ;
    cin.ignore();
    getline( cin, hometown2) ;
    if ( hometown.size() > 6)
    {
        cout << "That's a long name!" << endl;
    }
    cout << "Enter your name: " ;
    cin >> name ;
    cout << "Hello, " << name << ", from " << hometown << " " << hometown2 << "!" ;
}
void Program2()
{
    float userPoints ;
    float totalPoints ;
    float grade ;
    cout << "How many points does the assignment have?  " ;
    cin >> totalPoints ;
    cout << "How many points did you get?               " ;
    cin >> userPoints ;
    cout << "Score:                                     " << (userPoints / totalPoints)*100 << endl;
    if ((userPoints / totalPoints)*100 >= 60)
    {
        cout << "You passed!";
    }
    else
    {
       cout << "You failed!" ;
    }

}
void Program3()
{
    int charge ;
    cout << "Enter your phone charge:  " ;
    cin >> charge ;
    if (charge >= 75)
    {
        cout << "****" ;
    }
    else if (charge >= 50)
    {
        cout << "***_" ;
    }
    else if (charge >= 25)
    {
        cout << "**__" ;
    }
    else if (charge >= 5)
    {
        cout << "*___" ;
    }
    else
    {
        cout << "____" ;
    }
}
void Program4()
{
   int userChoice ;
   cout << "What is your favourite type of book?" << endl;
   cout << "1. Scifi" << endl;
   cout << "2. Historical" << endl;
   cout << "3. Fantasy" << endl;
   cout << "4. DIY" << endl;
   cout << " " << endl;
   cout << "Your selection:  " ;
   cin >> userChoice ;
   if (userChoice >= 1 && userChoice <= 4)
   {
       cout << "Good choice!" ;
   }
   else
    {
        cout << "Invalid choice!" ;
    }

}
void Program5()
{
    float num1 ;
    float num2 ;
    float result ;
    char operation ;
    cout << "Enter first number:     " ;
    cin >> num1 ;
    cout << "Enter second number:    " ;
    cin >> num2 ;
    cout << " " << endl;
    cout << "Which type of operation?" << endl ;
    cout << "+:  add" << endl ;
    cout << "-:  subtract" << endl ;
    cout << "*:  multiply" << endl ;
    cout << "/:  divide" << endl ;
    cout << " " << endl ;
    cout << "Choice:  " ;
    cin >> operation ;
    cout << " " << endl ;
    switch ( operation )
    {
        case '+':
            result = num1 + num2 ;
            break ;

        case '-':
            result = num1 - num2 ;
            break ;

        case '*':
            result = num1 * num2 ;
            break ;

        case '/':
            if (num2 == 0)
            {
                cout << "Can't divide by 0!" << endl;
            }
            else
            {
                result = num1 / num2 ;
            }
            break ;

        default:
            cout << "Unknown option!" ;

    }

        cout << "Result:  " << result ;

}
// Don't modify main
int main()
{
    while ( true )
    {
        cout << "Run which program? (1-5): ";
        int choice;
        cin >> choice;
        cout << endl << endl;
        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        cout << endl << "------------------------------------" << endl;
    }
    return 0;
}
